//
//  GitHubAPI.swift
//  SygicAssignment
//
//  Created by Denis Kakačka on 31/05/2018.
//  Copyright © 2018 Denis Kakačka. All rights reserved.
//

import Foundation
import Moya

enum GitHubRequests {
    case appleRepos
    case searchRepo(query: String)
}


extension GitHubRequests: TargetType {
    var baseURL: URL {
        return URL(string: "https://api.github.com")!
    }
    
    var path: String {
        switch self {
        case .appleRepos:
            return "/orgs/apple/repos"
        case .searchRepo:
            return "/search/repositories"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .appleRepos, .searchRepo:
            return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        case .searchRepo:
            return MockResponse.fromJSONFile("SearchReposMockJSON")
        case .appleRepos:
            return MockResponse.fromJSONFile("AppleReposMockJSON")
        }
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .appleRepos:
            return nil
        case .searchRepo(let query):
            return ["q": query.URLEscapedString as AnyObject]
        }
    }
    
    
}
