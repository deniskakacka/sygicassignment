//
//  GetAppleRepositoriesTask.swift
//  SygicAssignment
//
//  Created by Denis Kakačka on 29/05/2018.
//  Copyright © 2018 Denis Kakačka. All rights reserved.
//

import Foundation

class GetAppleRepositoriesTask: Operation {
    typealias Output = RepositoryModel
    
    var request: Request {
        return RepositoriesRequest.appleRepos
    }
}
