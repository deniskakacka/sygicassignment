//
//  Operation.swift
//  SygicAssignment
//
//  Created by Denis Kakačka on 29/05/2018.
//  Copyright © 2018 Denis Kakačka. All rights reserved.
//

import Foundation
import RxSwift

protocol Operation {
    associatedtype Output
    
    var request: Request { get }
    func perform(in dispatcher: Dispatcher) -> Single<Output>
}
