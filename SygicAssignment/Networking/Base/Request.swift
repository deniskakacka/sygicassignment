//
//  Request.swift
//  SygicAssignment
//
//  Created by Denis Kakačka on 29/05/2018.
//  Copyright © 2018 Denis Kakačka. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get
}

enum DataType {
    case data
    case JSON
}

enum RequestParameters {
    case body(_ : [String : Any]?)
    case url(_ : [String : Any]?)
}

protocol Request {
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: RequestParameters? { get }
    var headers: [String : Any]? { get }
    var dataType: DataType { get }
}

extension Request {
    var parameters: RequestParameters? { return nil }
    var headers: [String : Any]? { return nil }
}
